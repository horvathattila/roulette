#pragma once
#include <vector>
#include "BetName.h"
#include "Bet.h"

class Game
{
	static const int START_UP_CREDIT = 100;


	struct Player
	{
		int balance = START_UP_CREDIT;
		std::vector<Bet> bets;
		
	};


	Player player;
	int winningNumber;

	
public:
	Game();
	~Game();

	void setup();
	bool canContinue = true;

	bool canBet();
	bool wantToBet();
	bool gameIsOn();
	Bet createBet();
	void doRound();
	void showBalance();

	void askForBetAmount();
	void askForBetChoice();
	 int askForLowestPosition();
	void announceBetsBeforeRolling();
	void getWinningNumber();
	void checkForWinnings();
	void anonuceWinnings();

};


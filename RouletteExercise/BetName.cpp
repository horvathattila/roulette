#include "stdafx.h"
#include "BetName.h"


BetName::BetName()
{
}


BetName::~BetName()
{
}

//this fucntion returns the name of the bet type belongs to the curretn BetName object
std::string BetName::getBetName()
{
	
		std::string value;

		switch (id)
		{
		case	RED:			value = "RED";
			break;
		case	BLACK:			value = "BLACK";
			break;
		case	EVEN:			value = "EVEN";
			break;
		case    ODD:			value = "ODD";
			break;
		case	LOW_BET:		value = "LOW BET";
			break;
		case    HIGH_BET:		value = "HIGH BET";
			break;
		case	FIRST_DOZEN:	value = "FIRST DOZEN";
			break;
		case    MIDDLE_DOZEN:	value = "MIDDLE DOZEN";
			break;
		case	LAST_DOZEN:		value = "LAST DOZEN";
			break;
		case	COLUMN_BET:		value = "COLUMN BET";
			break;
		case	LINE_BET:		value = "LINE BET";
			break;
		case	CORNER_BET:		value = "CORNER BET";
			break;
		case	STREET_BET:		value = "STREET BET";
			break;
		case	SPLIT_BET:		value = "SPLIT BET";
			break;
		case	STRAIGHT_UP:	value = "STRAIGHT UP";
			break;
		default:value = "ERROR";
		}

		return value;
}

//this function returns the name of the bet type that belongs to a specific BetName 
std::string BetName::getBetNameFromId(BetId ID)
{

	std::string value;

	switch (ID)
	{
	case	RED:			value = "RED";
		break;
	case	BLACK:			value = "BLACK";
		break;
	case	EVEN:			value = "EVEN";
		break;
	case    ODD:			value = "ODD";
		break;
	case	LOW_BET:		value = "LOW BET";
		break;
	case    HIGH_BET:		value = "HIGH BET";
		break;
	case	FIRST_DOZEN:	value = "FIRST DOZEN";
		break;
	case    MIDDLE_DOZEN:	value = "MIDDLE DOZEN";
		break;
	case	LAST_DOZEN:		value = "LAST DOZEN";
		break;
	case	COLUMN_BET:		value = "COLUMN BET";
		break;
	case	LINE_BET:		value = "LINE BET";
		break;
	case	CORNER_BET:		value = "CORNER BET";
		break;
	case	STREET_BET:		value = "STREET BET";
		break;
	case	SPLIT_BET:		value = "SPLIT BET";
		break;
	case	STRAIGHT_UP:	value = "STRAIGHT UP";
		break;
	default:value = "ERROR";
	}

	return value;
}

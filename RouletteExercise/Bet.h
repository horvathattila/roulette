#pragma once
#include <vector>
class Bet
{
protected:	
	std::vector<int> winningNumbers;
	int ods;
	const char *name = "BET";
public:
	
	Bet();
	~Bet();
	int betAmount;
	int winAmount;
	int getWinAmount(int winningNumber);
	void setBetAmount(int amount);
	const char *getName();
};

class Red_Bet : public Bet
{

public:
	Red_Bet()
	{
		winningNumbers = { 1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36 };
		name = "RED_BET";
		ods = 1;
	}

};

class Black_Bet : public Bet
{

public:
	Black_Bet()
	{
		winningNumbers = { 2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35 };
		name = "BLACK_BET";
		ods = 1;
	}

};

class Even_Bet : public Bet
{

public:
	Even_Bet()
	{
		winningNumbers = { 0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36 };
		name = "EVEN_BET";
		ods = 1;
	}

};

class Odd_Bet : public Bet
{

public:
	Odd_Bet()
	{
		winningNumbers = { 1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35 };
		name = "ODD_BET";
		ods = 1;
	}

};

class Low_Bet : public Bet
{

public:
	Low_Bet()
	{
		winningNumbers = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18 };
		name = "LOW_BET";
		ods = 1;
	}

};
class High_Bet : public Bet
{

public:
	High_Bet()
	{
		winningNumbers = { 19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36 };
		name = "HIGH_BET";
		ods = 1;
	}

};

class First_Dozen_Bet : public Bet
{

public:
	First_Dozen_Bet()
	{
		winningNumbers = { 1,2,3,4,5,6,7,8,9,10,11,12 };
		name = "FIRST_DOZEN_BET";
		ods = 2;
	}

};

class Middle_Dozen_Bet : public Bet
{

public:
	Middle_Dozen_Bet()
	{
		winningNumbers = { 13,14,15,16,17,18,19,20,21,22,23,24 };
		name = "MIDDLE_DOZEN_BET";
		ods = 2;
	}

};

class Last_Dozen_Bet : public Bet
{

public:
	Last_Dozen_Bet()
	{
		winningNumbers = { 25,26,27,28,29,30,31,32,33,34,35,36 };
		name = "LAST_DOZEN_BET";
		ods = 2;
	}

};

class Column_Bet : public Bet
{

public:
	Column_Bet(int lowestPosition)
	{
		if (lowestPosition == 1)
		{
			winningNumbers = { 1,4,7,10,13,16,19,22,25,28,31,34 };
		}
		else if (lowestPosition == 2)
		{
			winningNumbers = { 2,5,8,11,14,17,20,23,26,29,32,35 };
		}
		else if (lowestPosition == 3)
		{
			winningNumbers = { 3,6,9,12,15,18,21,24,27,30,33,36 };
		}
		name = "COLUMN_BET";
		ods = 2;
	}

};

class Line_Bet : public Bet
{

public:
	Line_Bet(int lowestPosition)
	{
		winningNumbers = { lowestPosition , lowestPosition + 1, lowestPosition + 2, lowestPosition + 3, lowestPosition + 4, lowestPosition + 5 };
		name = "LINE_BET";
		ods = 5;
	}

};

class Corner_Bet : public Bet
{

public:
	Corner_Bet(int lowestPosition)
	{
		winningNumbers = { lowestPosition, lowestPosition + 1, lowestPosition + 3, lowestPosition + 4 };
		name = "CORNER_BET";
		ods = 8;
	}

};

class Street_Bet : public Bet
{

public:
	Street_Bet(int lowestPosition)
	{
		winningNumbers = { lowestPosition , lowestPosition + 1, lowestPosition + 2 };
		name = "STREET_BET";
		ods = 11;
	}

};

class Split_Bet : public Bet
{

public:
	Split_Bet(int lowestPosition)
	{
		winningNumbers = { lowestPosition };
		name = "SPLIT_BET";
		ods = 17;
	}

};

class Straight_Up_Bet : public Bet
{

public:
	Straight_Up_Bet(int lowestPosition)
	{
		winningNumbers = { lowestPosition };
		name = "STRAIGHT_UP_BET";
		ods = 35;
	}

};
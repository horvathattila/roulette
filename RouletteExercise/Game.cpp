#include "stdafx.h"
#include "Game.h"

#include <iostream>

#include <random>

#define RAND_MAX 37;

Bet bet;
int betAmount;
int betChoice;
bool justStarted = true;


Game::Game()
{
}


Game::~Game()
{
}

void Game::setup()
{
	player = Player();
}

bool Game::canBet()
{   
	showBalance();

	if (player.balance <= 0)
	{
		std::cout << "YOU CAN NOT PLACE MORE BETS \n";
		canContinue = false; 
		return false;
	}
	else return true;
}

bool Game::wantToBet()
{
	

		char answer;
	start:
		showBalance();

		if (justStarted)
		{

			std::cout << "Would you like to place a bet? Y/N \n";
			justStarted = false;
		}
		else
		{

			std::cout << "Would you like to place more bets? Y/N \n";

		}
		std::cin >> answer;

		if (answer == 'y' || answer == 'Y') return true;
		else if (answer == 'n' || answer == 'N') return false;

		else
		{
			std::cout << "Wrong answer please choose Y or N \n";
			goto start;
		}


}

void Game::askForBetAmount()
{
start:
	std::cout << "How much money would you like to bet?\n";
	std::cin >> betAmount;
	if (betAmount > player.balance)
	{
		std::cout << "Sorry you dont have sufficient founds for this bet \n Please try again \n";
		goto start;
	}
	player.balance -= betAmount;
}

void Game::askForBetChoice()
{
start:
	std::cout << "What would you like to bet on? \n ";
	BetName type;
	for (int x = 0; x < BetName::MAX; x++)
	{
		std::cout << x << '-' << type.getBetNameFromId((BetName::BetId)x) << "\n";
	}
	std::cin >> betChoice;
	if (!(betChoice >= 0 && betChoice <= 14))
	{
		std::cout << "ERROR TRY AGAIN \n";
		goto start;
	}
}

int Game::askForLowestPosition()
{
	int lowestPosition;
	std::cout << "What is the lowest position of your bet?\n";
	std::cin >> lowestPosition;
	return lowestPosition;
}


Bet Game::createBet()
{
	int position;
	switch (betChoice)
	{
	case 0:
		bet = Red_Bet();
		bet.betAmount = betAmount;
		break;
	case 1:
		bet = Black_Bet();
		bet.betAmount = betAmount;
		break;
	case 2:
		bet = Even_Bet();
		bet.betAmount = betAmount;
		break;
	case 3:
		bet = Odd_Bet();
		bet.betAmount = betAmount;
		break;
	case 4:
		bet = Low_Bet();
		bet.betAmount = betAmount;
		break;
	case 5:
		bet = High_Bet();
		bet.betAmount = betAmount;
		break;
	case 6:
		bet = First_Dozen_Bet();
		bet.betAmount = betAmount;
		break;
	case 7:
		bet = Middle_Dozen_Bet();
		bet.betAmount = betAmount;
		break;
	case 8:
		bet = Last_Dozen_Bet();
		bet.betAmount = betAmount;
		break;
	case 9:
		position = askForLowestPosition();
		bet = Column_Bet(position);
		bet.betAmount = betAmount;
		break;
	case 10:
		position = askForLowestPosition();
		bet = Line_Bet(position);
		bet.betAmount = betAmount;
		break;
	case 11:
		position = askForLowestPosition();
		bet = Corner_Bet(position);
		bet.betAmount = betAmount;
		break;
	case 12:
		position = askForLowestPosition();
		bet = Street_Bet(position);
		bet.betAmount = betAmount;
		break;
	case 13:
		position = askForLowestPosition();
		bet = Split_Bet(position);
		bet.betAmount = betAmount;
		break;
	case 14:
		position = askForLowestPosition();
		bet = Straight_Up_Bet(position);
		bet.betAmount = betAmount;
		break;
	default:
		break;
	}
	return bet;
}

void Game::checkForWinnings()
{
	for (int i = 0; i < player.bets.size(); i++)
	{
		player.balance += player.bets[i].getWinAmount(winningNumber);
	}
}

void Game::anonuceWinnings()
{
	std::cout << "The winning number is : " << winningNumber << "\n";


	for (int i = 0; i < player.bets.size(); i++)
	{
		std::cout << "You have Won: " << player.bets[i].winAmount << " With " << player.bets[i].getName() << "\n";
	}

	
}

void Game::announceBetsBeforeRolling()
{
	std::cout << "\n YOUR BETS ARE: \n ";

	for (int i = 0; i < player.bets.size(); i++)
	{
		std::cout << player.bets[i].betAmount << " on " << player.bets[i].getName() << " \n";
	}

}

void Game::getWinningNumber()
{
	
	std::mt19937 rng;
	rng.seed(std::random_device()());
	std::uniform_int_distribution<std::mt19937::result_type> dist6(0, 37);

	winningNumber = dist6(rng); 

}

bool Game::gameIsOn()
{
	return canContinue;
}

void Game::doRound()
{
	
	player.bets.clear();

	while (wantToBet() && canBet())
	{ 
		
		askForBetAmount();
		askForBetChoice();
		Bet bet = createBet();
		player.bets.push_back(bet);
	}

		
	announceBetsBeforeRolling();
	getWinningNumber();
	checkForWinnings();
	anonuceWinnings();
	
}

void Game::showBalance()
{
	std::cout << "YOUR BALANCE IS: " << player.balance << "\n";
}








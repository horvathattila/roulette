#pragma once
#include <string>

class BetName
{
		

public:

	enum BetId
	{
		RED ,
		BLACK,
		EVEN,
		ODD,
		LOW_BET,
		HIGH_BET,
		FIRST_DOZEN,
		MIDDLE_DOZEN,
		LAST_DOZEN,
		COLUMN_BET,
		LINE_BET,
		CORNER_BET,
		STREET_BET,
		SPLIT_BET,
		STRAIGHT_UP,
		MAX
	};


	BetId id;

	BetName();
	~BetName();
	std::string getBetName();
	std::string getBetNameFromId(BetId ID);
};


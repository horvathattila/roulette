// RouletteExercise.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Game.h"


int main()
{
	Game thisGame = Game::Game();

	thisGame.setup();

	while (thisGame.gameIsOn())
	{
		thisGame.doRound();
	}

	while (1);

    return 0;
}

